from django.db import models

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Application(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=250)
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    student_id = models.IntegerField(null=True)
    year = models.IntegerField(null=True)
    hostel_name = models.CharField(max_length=250)
    room_number = models.IntegerField(null=True)
    joined_date = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=False)
    receipt = models.ImageField(upload_to='receipts/')

    def __str__(self):
        return self.name


@receiver(post_save, sender=User)
def update_user_application(sender, instance, created, **kwargs):
    if created:
        Application.objects.create(user=instance)
    instance.application.save()
