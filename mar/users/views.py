from django.db import transaction
from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

from .forms import SignUpForm, ApplicationForm


# Create your views here.
def index(request):
    context = {'name': 'he'}
    return render(request, 'users/index.html', context)


@transaction.atomic
def apply_accommodation(request):
    if request.method == 'POST':
        user_form = SignUpForm(request.POST)
        application_form = ApplicationForm(request.POST, request.FILES)

        if user_form.is_valid() and application_form.is_valid():
            user = user_form.save()
            user.refresh_from_db()   # Loads Profile created by signals
            # Reload the profile form with profile instance

            application_form = ApplicationForm(request.POST, request.FILES,
             instance=user.application)
            application_form.full_clean()
            application_form.save()
            raw_password = user_form.cleaned_data.get('password1')

            user = authenticate(request, username=user.username,
                                password=raw_password)
            login(request, user)
            return redirect('index');
    else:
        user_form = SignUpForm()
        application_form = ApplicationForm()

    return render(request, 'users/register.html', {
        'user_form': user_form,
        'application_form': application_form
    })